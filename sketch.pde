
long[] timings   = {1,1};//{10,4};//{100,100,100,100,100,100,100,100,400,400};
int[] amplitudes = {0,255};//{ 63,127,191,255,191,127, 63,  0,127,  0};

//  11  09     06  04  02
//12  10  08 07  05  03  01
//  --  --    10  --  --    -3  01
//--  --  ----  08  --  0403  02  -1

//1拍=10(柴又bpm)
int[][] map = {
  {28,5},
  {2,0},
  {27,7},
  {3,0},
  {10,11},
  {10,7},
  {17,5},//
  {3,0},
  {10,5},
  {27,7},
  {3,0},
  {10,11},
  {10,12},
  {7,13},//
  {3,0},
  {2,13},
  {3,0},
  {2,13},
  {3,0},
  {7,13},
  {3,0},
  {10,15},
  {17,13},
  {3,0},
  {17,7},
  {3,0},
  {7,11},//
  {3,0},
  {2,11},
  {3,0},
  {2,11},
  {3,0},
  {2,11},
  {3,0},
  {2,11},
  {3,0},
  {7,11},
  {3,0},
  {17,15},
  {3,0},
  {17,16},
  {3,0},
  {7,15},//
  {3,0},
  {2,15},
  {3,0},
  {2,15},
  {3,0},
  {10,15},
  {10,13},
  {17,11},
  {3,0},
  {17,7},
  {3,0},
  {7,5},//
  {3,0},
  {2,5},
  {3,0},
  {2,5},
  {3,0},
  {10,5},
  {10,11},
  {17,7},
  {3,0},
  {17,11},
  {3,0},
  {17,5},//
  {3,0},
  {10,5},
  {10,11},
  {17,4},
  {3,0},
  {10,5},
  {10,4},
  {10,3},//
  {10,4},
  {10,7},
  {10,13},
  {17,15},
  {3,0},
  {17,4},
  {3,0},
};
int i = 0;
int nextSoundChange = 1;


void setup(){
   i = 0;
   nextSoundChange = 1;
   frameCount = 0;
   frameRate(57);
}
void draw(){
  background(255);
  //vibrate(timings, amplitudes,0);
  
  //絶対
  //for(int i=0;i<map.length;i++)if(frameCount==map[i][0])vibChange(map[i][1]);
  
  //相対
  if(frameCount==nextSoundChange){
    vibChange(map[i][1]);
    nextSoundChange += map[i][0];
    if(i+1<map.length)i++;
    else setup();
  }
}
/*
boolean t =false;
int touchI = 1;
void mousePressed(){
  if(!t){
    t = true;
    timings[1] = touchI;
    //Vib.vibrate(VibrationEffect.createOneShot(500,255));
    Vib.vibrate(VibrationEffect.createWaveform(timings,amplitudes,0));
  }else{
    t= false;
    Vib.cancel();
    touchI++;
  }
}
*/
void vibChange(int n){
  if(n==0)stopVibrate();
  else{
    timings[0] = n+8;
    vibrate(timings, amplitudes,0);
  }
}
