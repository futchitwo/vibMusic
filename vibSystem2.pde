import android.os.Vibrator;
import android.content.Context;
import android.os.VibrationEffect;
//Vibrator VibratorContext = (Vibrator)getContext().getSystemService(Context.VIBRATOR_SERVICE); 

long[] intArr2longArr(int[] a){
  long[] a2 = new long[a.length];
  for(int i=0;i<a.length;i++){
    a2[i] = (int) a[i];
  }
  return a2;
}

//t:timing
//a:amplitede(range:0-255, default:-1)
//e:repeat(-1 is no repeat)

void vibrate(int t, int a){
  ((Vibrator)getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(t,a));
}
void vibrate(int t){
  vibrate(t,-1);
}

void vibrate(long[] t, int[] a, int r){
  ((Vibrator)getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(VibrationEffect.createWaveform(t,a,r));
}
void vibrate(int[] t, int[] a, int r){
  vibrate(intArr2longArr(t),a,r);
}

void vibrate(long[] t, int r){
  ((Vibrator)getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(VibrationEffect.createWaveform(t,r));
}
void vibrate(int[] t, int r){
  vibrate(intArr2longArr(t),r);
}

void stopVibrate(){
  ((Vibrator)getContext().getSystemService(Context.VIBRATOR_SERVICE)).cancel();
}